hamdi
=====

Projet symfony2 avec une couche REST API. 
## Prerequisites ##
* php >=5.3.9;
* Composer doit être installer;
* Git doit être installer.
## Installation ##
* Télécharger le repository

```
#!git

git clone https://tibou@bitbucket.org/tibou/hamdi.git
```
* Installer les dépendances

```
#!composer

sudo composer update
```
* Créer la base de données "hamdi"

```
#!console

php  app/console doctrine:database:create
```
* Créer le schema de la BD

```
#!console

php app/console doctrine:schema:update --force
```

* Remplir la BD avec des données d'exemples

```
#!console

php app/console doctrine:fixtures:load
```
* Visualiser le projet  
[http://localhost/hamdi/web/app_dev.php/admin/post/](http://localhost/hamdi/web/app_dev.php/admin/post/)
* S'authentifier  
Login       : hamdi  
mot de passe: 123456
## Api ##
### Post ###
GET Posts : [http://localhost/hamdi/web/app_dev.php/api/posts](http://localhost/hamdi/web/app_dev.php/api/posts)
### Comment ###
GET Comments : [http://localhost/hamdi/web/app_dev.php/api/comments](http://localhost/hamdi/web/app_dev.php/api/comments)