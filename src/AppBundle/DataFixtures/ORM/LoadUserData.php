<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        $test_password = '123456';

        $factory = $this->container->get('security.encoder_factory');
        /** @var $manager \FOS\UserBundle\Doctrine\UserManager */
        $manager = $this->container->get('fos_user.user_manager');
        /** @var $user \Application\Sonata\UserBundle\Entity\User */
        $user = $manager->createUser();
        $user->setUsername('hamdi');
        $user->setEmail('hamdi@live.com');
        $user->setRoles(array('ROLE_SUPER_ADMIN'));
        $user->setEnabled(true);
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($test_password, $user->getSalt());
        $user->setPassword($password);

        $user1 = $manager->createUser();
        $user1->setUsername('user');
        $user1->setEmail('user@gmail.com');
        $user1->setRoles(array('ROLE_USER'));
        $user1->setEnabled(true);
        $encoder2 = $factory->getEncoder($user1);
        $password2 = $encoder2->encodePassword($test_password, $user1->getSalt());
        $user1->setPassword($password2);

        $manager->updateUser($user);
        $manager->updateUser($user1);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder() {
        return 3;
    }

}
