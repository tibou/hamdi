<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Comment;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadCommentData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $comment = new Comment();
        $comment->setContent("post1#comment1");
        $comment->setCreatedAt(new \DateTime('now'));
        $comment->setPost($this->getReference('london'));

        $comment1 = new Comment();
        $comment1->setContent("post1#comment2");
        $comment1->setCreatedAt(new \DateTime('now'));
        $comment1->setPost($this->getReference('london'));

        $manager->persist($comment);
        $manager->persist($comment1);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder() {
        return 2;
    }

}
