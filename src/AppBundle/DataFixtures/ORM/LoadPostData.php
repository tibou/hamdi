<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Post;

class LoadPostData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $post = new Post();
        $post->setCreatedAt(new \DateTime('now'));
        $post->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                . " Curabitur consequat porta risus, non facilisis leo vestibulum vitae."
                . " Pellentesque eu orci nisi.");
        $post->setImageURL("http://cdn.ek.aero/english/images/London-1_tcm233-2111842.jpg");
        $post->setTitle("London");

        $manager->persist($post);
        $manager->flush();

        $this->addReference('london', $post);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder() {
        return 1;
    }

}
