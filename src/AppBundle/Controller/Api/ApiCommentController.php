<?php

namespace AppBundle\Controller\Api;

header('Access-Control-Allow-Origin: *');

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Comment;
use Symfony\Component\HttpFoundation\Request;

/**
 * Api Comment controller.
 */
class ApiCommentController extends FOSRestController {

    /**
     * List All Comments
     * 
     * @Rest\Get("api/comments", name="api_comments", options={ "method_prefix" = false })
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository('AppBundle:Comment')->findAll();

        return View::create($comments, '200');
    }

    /**
     * Add a Comment
     * 
     * @Rest\Post("api/comments", name="api_new_comment", 
     * options={ "method_prefix" = false })
     */
    public function newAction(Request $request) {
        $comment = new Comment();
        $form = $this->createForm('AppBundle\Form\CommentType', $comment);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $view = new View();
            $view->setStatusCode('201');
            // set the `Location` header only when creating new resources
            $url = $this->generateUrl('api_comment', array(
                'id' => $comment->getId()), true
            );
            $view->setHeader('Location', $url);

            return $this->handleView($view);
        }

        return View::create($form, 400);
    }

    /**
     * Finds and displays a Comment entity.
     * 
     * @Rest\Get("api/comments/{id}", name="api_comment", 
     * requirements={"id" = "\d+"}, options={ "method_prefix" = false })
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('AppBundle:Comment')->find($id);

        if (!$comment instanceof Comment) {

            return View::create(NULL, '404');
        }

        return View::create($comment, '200');
    }

    /**
     * Edit an existing Comment
     * 
     * @Rest\Put("api/comments/{id}", name="api_update_comment", 
     * options={ "method_prefix" = false })
     */
    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('AppBundle:Comment')->find($id);
        if (!$comment instanceof Comment) {
            return View::create(NULL, '404');
        }
        $form = $this->createForm(new \AppBundle\Form\CommentType(), $comment);
        $form->handleRequest($request);
        $form->submit($request);

        if ($form->isValid()) {
            $em->persist($comment);
            $em->flush();

            return View::create(NULL, '204');
        }

        return View::create($form, 400);
    }

    /**
     * Delete a Comment
     *
     * @Rest\Delete("api/comments/{id}", name="api_delete_comment", 
     * requirements={"id" = "\d+"}, options={ "method_prefix" = false })
     */
    public function processDelete($id) {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('AppBundle:Comment')->find($id);

        if (!$comment instanceof Comment) {

            return View::create(NULL, '404');
        }

        $em->remove($comment);
        $em->flush();

        return View::create(NULL, '204');
    }

}
