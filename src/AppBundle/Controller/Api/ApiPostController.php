<?php

namespace AppBundle\Controller\Api;

header('Access-Control-Allow-Origin: *');

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Request;

/**
 * Api Post controller.
 */
class ApiPostController extends FOSRestController {

    /**
     * List All Posts
     * 
     * @Rest\Get("api/posts", name="api_posts", options={ "method_prefix" = false })
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('AppBundle:Post')->findAll();

        return View::create($posts, '200');
    }

    /**
     * Add a Post
     * 
     * @Rest\Post("api/posts", name="api_new_post", 
     * options={ "method_prefix" = false })
     */
    public function newAction(Request $request) {
        $post = new Post();
        $form = $this->createForm('AppBundle\Form\PostType', $post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $view = new View();
            $view->setStatusCode('201');
            // set the `Location` header only when creating new resources
            $url = $this->generateUrl('api_post', array(
                'id' => $post->getId()), true
            );
            $view->setHeader('Location', $url);

            return $this->handleView($view);
        }

        return View::create($form, 400);
    }

    /**
     * Finds and displays a Post entity.
     * 
     * @Rest\Get("api/posts/{id}", name="api_post", 
     * requirements={"id" = "\d+"}, options={ "method_prefix" = false })
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->find($id);

        if (!$post instanceof Post) {

            return View::create(NULL, '404');
        }

        return View::create($post, '200');
    }

    /**
     * Edit an existing Post
     * 
     * @Rest\Put("api/posts/{id}", name="api_update_post", 
     * options={ "method_prefix" = false })
     */
    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->find($id);
        if (!$post instanceof Post) {
            return View::create(NULL, '404');
        }
        $form = $this->createForm(new \AppBundle\Form\PostType(), $post);
        $form->handleRequest($request);
        $form->submit($request);

        if ($form->isValid()) {
            $em->persist($post);
            $em->flush();

            return View::create(NULL, '204');
        }

        return View::create($form, 400);
    }

    /**
     * Delete a Post
     *
     * @Rest\Delete("api/posts/{id}", name="api_delete_post", 
     * requirements={"id" = "\d+"}, options={ "method_prefix" = false })
     */
    public function processDelete($id) {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->find($id);

        if (!$post instanceof Post) {

            return View::create(NULL, '404');
        }

        $em->remove($post);
        $em->flush();

        return View::create(NULL, '204');
    }

}
